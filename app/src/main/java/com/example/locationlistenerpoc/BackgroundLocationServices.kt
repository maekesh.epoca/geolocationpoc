package com.example.locationlistenerpoc

import android.Manifest
import android.annotation.SuppressLint
import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import android.widget.Toast
import androidx.core.app.ActivityCompat

class BackgroundLocationServices : Service() {
    private val TAG = "BgLocationServices"
    private var locationManager: LocationManager ?= null
    private val LOCATION_DISTANCE = 1f
    private lateinit var mLocationListeners: Array<BgLocationListener>

    override fun onBind(p0: Intent?): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        super.onStartCommand(intent, flags, startId)
        return START_STICKY
    }

    @SuppressLint("MissingPermission")
    override fun onCreate() {
        var context = applicationContext
        mLocationListeners = arrayOf<BgLocationListener>(
            BgLocationListener(LocationManager.PASSIVE_PROVIDER, context)
        )

        initializeLocationManager()

        if (isPermissionGranted()){
            locationManager?.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER, 1000, LOCATION_DISTANCE, mLocationListeners[0])
        } else {
            Log.d(TAG, "Permission missed")
        }

    }

    private fun isPermissionGranted() : Boolean {

        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return false
        }

        return true
    }

    private fun initializeLocationManager() {
        locationManager = application.getSystemService(Context.LOCATION_SERVICE) as LocationManager
    }

    override fun onDestroy() {
        super.onDestroy()

        if (locationManager != null) {
            for (element in mLocationListeners) {
                try {
                    if (isPermissionGranted()){
                        locationManager!!.removeUpdates(element)
                    } else {
                        Log.d(TAG, "Permission missed")
                    }
                } catch (ex: Exception) {
                    Log.i(TAG, "fail to remove location listener, ignore", ex)
                }
            }
        }
    }

    class BgLocationListener(
        passiveProvider: String,
        applicationContext: Context
    ) : LocationListener {
         var context = applicationContext
        override fun onLocationChanged(latLng: Location?) {
            Log.d("location Listener", latLng.toString())
            Toast.makeText(context, latLng.toString(), Toast.LENGTH_LONG).show()
        }

        override fun onStatusChanged(p0: String?, p1: Int, p2: Bundle?) {
            Log.d("location Listener", "onStatusChanged")
        }

        override fun onProviderEnabled(p0: String?) {
            Log.d("location Listener", "onProviderEnabled")
            Toast.makeText(context, "enabled", Toast.LENGTH_LONG).show()
        }

        override fun onProviderDisabled(p0: String?) {
            Log.d("location Listener", "onProviderDisabled")
            Toast.makeText(context, "disabled", Toast.LENGTH_LONG).show()
        }
    }
}