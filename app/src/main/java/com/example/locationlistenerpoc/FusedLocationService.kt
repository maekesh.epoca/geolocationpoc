package com.example.locationlistenerpoc

import android.annotation.SuppressLint
import android.app.Service
import android.content.Context
import android.content.Intent
import android.location.Location
import android.os.IBinder
import android.widget.Toast
import com.google.android.gms.location.*
import com.google.android.gms.tasks.OnCompleteListener

class FusedLocationService : Service() {

    private lateinit var location: Location
    private lateinit var fusedLocationProvider: FusedLocationProviderClient
    private lateinit var locationCallback: LocationCallback

    override fun onBind(p0: Intent?): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        super.onStartCommand(intent, flags, startId)
        return START_STICKY
    }

    override fun onCreate() {
        super.onCreate()
        getLocationUpdate(contexts = applicationContext)
    }

    @SuppressLint("MissingPermission")
    private fun getLocationUpdate(contexts: Context) {

        fusedLocationProvider = LocationServices.getFusedLocationProviderClient(contexts)
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult) {
                super.onLocationResult(locationResult)
            }
        }

        val locationRequest = LocationRequest()
        locationRequest.interval = 10000
        locationRequest.fastestInterval = 10000/2
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY

        fusedLocationProvider.lastLocation.addOnCompleteListener(OnCompleteListener {
            if (it.isSuccessful && it.result !=null){
                location = it.result!!
                Toast.makeText(contexts, "location received are: " +location.latitude+ " "+location.longitude, Toast.LENGTH_LONG).show()
                fusedLocationProvider.removeLocationUpdates(locationCallback)
            }
        })

        fusedLocationProvider.requestLocationUpdates(locationRequest, null)
    }
}