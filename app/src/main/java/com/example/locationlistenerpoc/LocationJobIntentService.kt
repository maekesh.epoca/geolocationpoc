package com.example.locationlistenerpoc

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.location.Location
import android.widget.Toast
import androidx.core.app.JobIntentService
import com.google.android.gms.location.*
import com.google.android.gms.tasks.OnCompleteListener

class LocationJobIntentService: JobIntentService() {

    private lateinit var location: Location
    private lateinit var fusedLocationProvider: FusedLocationProviderClient
    private lateinit var locationCallback: LocationCallback

    companion object {
        val JOB_ID = 2
        val TAG = "TAG"
        fun enqueueWork(context: Context, intent: Intent) {
            enqueueWork(context, LocationJobIntentService::class.java, JOB_ID, intent)
        }
    }


    override fun onHandleWork(intent: Intent) {
        getLocationUpdate(contexts = applicationContext)
    }

    @SuppressLint("MissingPermission")
    private fun getLocationUpdate(contexts: Context) {

        fusedLocationProvider = LocationServices.getFusedLocationProviderClient(contexts)
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult) {
                super.onLocationResult(locationResult)
            }
        }

        val locationRequest = LocationRequest()
        locationRequest.interval = 10000
        locationRequest.fastestInterval = 10000/2
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY

        fusedLocationProvider.lastLocation.addOnCompleteListener(OnCompleteListener {
            if (it.isSuccessful && it.result !=null){
                location = it.result!!
                Toast.makeText(contexts, "location received are: " +location.latitude+ " "+location.longitude, Toast.LENGTH_LONG).show()
                fusedLocationProvider.removeLocationUpdates(locationCallback)
            }
        })

        fusedLocationProvider.requestLocationUpdates(locationRequest, null)
    }
}