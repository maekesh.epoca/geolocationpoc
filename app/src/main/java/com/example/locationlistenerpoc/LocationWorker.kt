package com.example.locationlistenerpoc

import android.annotation.SuppressLint
import android.content.Context
import android.location.Location
import android.widget.Toast
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.google.android.gms.location.*
import com.google.android.gms.tasks.OnCompleteListener

class LocationWorker(context: Context, workerParams: WorkerParameters) : Worker(context,
    workerParams) {
    private val contexts = context
    private lateinit var location: Location
    private lateinit var fusedLocationProvider: FusedLocationProviderClient
    private lateinit var locationCallback: LocationCallback


    override fun doWork(): Result {
        //Here the location process will take place
        getLocationUpdate()
        return Result.success()
    }

    @SuppressLint("MissingPermission")
    private fun getLocationUpdate() {

        fusedLocationProvider = LocationServices.getFusedLocationProviderClient(contexts)
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult) {
                super.onLocationResult(locationResult)
            }
        }

        val locationRequest = LocationRequest()
        locationRequest.interval = 10000
        locationRequest.fastestInterval = 10000/2
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY

        fusedLocationProvider.lastLocation.addOnCompleteListener(OnCompleteListener {
            if (it.isSuccessful && it.result !=null){
                location = it.result!!
                Toast.makeText(contexts, "location received are: " +location.latitude+ " "+location.longitude, Toast.LENGTH_LONG).show()
                fusedLocationProvider.removeLocationUpdates(locationCallback)
            }
        })

        fusedLocationProvider.requestLocationUpdates(locationRequest, null)
    }
}