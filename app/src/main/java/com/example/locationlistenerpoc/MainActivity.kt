package com.example.locationlistenerpoc

import android.Manifest
import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.content.Intent
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.app.ActivityCompat
import androidx.work.PeriodicWorkRequestBuilder
import androidx.work.WorkManager
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity() {

    private val PERMISSION_REQUEST_CODE = 200;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (isPermissionGranted()) {
//            startService(Intent(this, BackgroundLocationServices::class.java))
//            startService(Intent(this, FusedLocationService::class.java))
//            getLcoationUpdate()
            startJobIntentService()
        } else {
            ActivityCompat.requestPermissions(this, arrayOf(ACCESS_FINE_LOCATION), PERMISSION_REQUEST_CODE);
        }
    }

    private fun startJobIntentService() {
        val intent = Intent(this, LocationJobIntentService::class.java)
        intent.putExtra("maxCountValue", 10)
        LocationJobIntentService.enqueueWork(this, intent)
    }

    private fun getLcoationUpdate() {
        val workRequest = PeriodicWorkRequestBuilder<LocationWorker>(
            repeatInterval = 15,
            repeatIntervalTimeUnit = TimeUnit.MINUTES
        ).build()

        WorkManager.getInstance(this).enqueue(workRequest)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (grantResults.isNotEmpty()
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                startService(Intent(this, BackgroundLocationServices::class.java))
//                startService(Intent(this, FusedLocationService::class.java))
//                getLcoationUpdate()
                startJobIntentService()
            }
        }
    }

    private fun isPermissionGranted() : Boolean {

        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return false
        }

        return true
    }
}